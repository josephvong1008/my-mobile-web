const path = require('path')
const basicConfig = require('@xdh/my/core/vue.config')

module.exports = {
  ...basicConfig,
  // 以下代码为定制VantUI 全局样式，其中 less-loader 必须为5.0版本；
  // 参考网址：https://blog.csdn.net/qq_32555123/article/details/105513209
   
  css: {
    extract: process.env.NODE_ENV === 'production', // ? false : true,
    sourceMap: true,
    loaderOptions: {
      less: {
        modifyVars: {
          // 使用 hack 方式 将 src/style/vant目录下的 自定义主题less文件替换VantUI原有的变量
          hack: `true; @import "${path.join(__dirname, './src/style/vant/theme.less')}"`
        }
      }
    }
  }
}