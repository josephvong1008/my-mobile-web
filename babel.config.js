// module.exports = require('@xdh/my/core/babel.config')
const basicConfig = require('@xdh/my/core/babel.config')
const vantImport = [
  'import', {
    libraryName: 'vant',
    libraryDirectory: 'es'
    // style: true
  }, 'vant'
]
basicConfig.plugins = basicConfig.plugins.concat([vantImport])
module.exports = basicConfig