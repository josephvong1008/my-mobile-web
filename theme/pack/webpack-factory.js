const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin')
const config = require('../config')
const utils = require('@xdh/my/core/utils')

module.exports = function (theme, entry, outDir) {
  return {
    mode: 'production',
    entry: entry,
    output: {
      filename: '[name].js',
      path: utils.join(outDir, theme.name, 'build')
    },
    resolve: {
      alias: {
        '@': utils.join(config.ProjectRootPath, 'src'), 
        $my: config.TempPath
      }
    },
    module: {
      rules: [
        {
          test: /\.less$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                hmr: false,
                publicPath: 'fonts/'
              }
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: false
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: false
              }
            },
            {
              loader: 'less-loader',
              options: {
                sourceMap: false,
                modifyVars: {
                  // 使用 hack 方式 将 src/style/vant目录下的 自定义主题less文件替换VantUI原有的变量
                  // hack: `true; @import "${path.join(__dirname, './src/style/vant/theme.less')}"`
                  hack: `true; @import "${utils.join(config.ProjectRootPath, '/src/style/vant/themes/' + theme.name + '.less')}"`
                }
              }
            },
            {
              // 试试用@xdh里面的themeVarScssLoder行不行
              loader: config.ThemeVarScssLoaderPath,
              options: {
                vars: utils.join(config.ProjectThemeVarPath, theme.file)
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '../index.css'
      }),
      new OptimizeCssnanoPlugin(
        {
          sourceMap: false,
          cssnanoOptions: {
            preset: [
              'default',
              {
                mergeLonghand: false,
                cssDeclarationSorter: false
              }
            ]
          }
        }
      )
    ]
  }
}